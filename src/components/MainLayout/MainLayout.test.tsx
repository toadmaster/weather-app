import renderer from 'react-test-renderer';
import { render, screen } from '@testing-library/react';
import MainLayout from '.';
import cities from '../../data/cities.json';

it('renders loader and matches snapshot', () => {
  const loader = renderer
    .create(
      <MainLayout selectCity={jest.fn} currentCity={cities[0]}>
        <span>Testing Layout</span>
      </MainLayout>,
    )
    .toJSON();
  expect(loader).toMatchSnapshot();
});

test('renders children prop appropriately', () => {
  render(
    <MainLayout selectCity={jest.fn} currentCity={cities[0]}>
      <span>Testing Layout</span>
    </MainLayout>,
  );

  const text = screen.getByText(/Testing Layout/i);

  expect(text).toBeInTheDocument();
});
