import Nav from '../Nav';

import './styles.scss';

interface MainLayoutProps {
  children: React.ReactNode;
  currentCity: City;
  selectCity: (city: City) => () => void;
}

const MainLayout = ({ children, selectCity, currentCity }: MainLayoutProps) => {
  return (
    <div className="app">
      <Nav currentCity={currentCity} selectCity={selectCity} />
      <main className="main-content">
        <div className="main-content__box">
          <div className="main-content__box__inner">{children}</div>
        </div>
      </main>
    </div>
  );
};

export default MainLayout;
