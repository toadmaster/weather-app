import classNames from 'classnames';
import cities from '../../data/cities.json';

import './styles.scss';

interface NavProps {
  currentCity: City;
  selectCity: (city: City) => () => void;
}

const Nav = ({ currentCity, selectCity }: NavProps) => {
  return (
    <nav className="main-nav">
      <ul className="main-nav__menu">
        {cities.map((c: City) => (
          <li
            key={c.name}
            className={classNames('main-nav__menu__item', {
              'main-nav__menu__item--active': c.name === currentCity.name,
            })}
          >
            <button onClick={selectCity(c)}>{c.label}</button>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Nav;
