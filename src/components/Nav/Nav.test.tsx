import renderer from 'react-test-renderer';
import { render, screen, fireEvent } from '@testing-library/react';
import Nav from '.';
import cities from '../../data/cities.json';

it('renders loader and matches snapshot', () => {
  const loader = renderer.create(<Nav selectCity={jest.fn} currentCity={cities[1]} />).toJSON();
  expect(loader).toMatchSnapshot();
});

it('renders the cities in nav', () => {
  render(<Nav selectCity={jest.fn} currentCity={cities[1]} />);
  const calgary = screen.getByText(/calgary/i);
  const amsterdam = screen.getByText(/amsterdam/i);
  const singapore = screen.getByText(/singapore/i);

  expect(calgary).toBeInTheDocument();
  expect(amsterdam).toBeInTheDocument();
  expect(singapore).toBeInTheDocument();
});

it('bolds currently active city (Amsterdam)', () => {
  render(<Nav selectCity={jest.fn} currentCity={cities[1]} />);
  const amsterdam = screen.getByText(/amsterdam/i);
  const calgary = screen.getByText(/calgary/i);

  expect(amsterdam.parentElement).toHaveClass('main-nav__menu__item--active');
  expect(calgary.parentElement).not.toHaveClass('main-nav__menu__item--active');
});

it('clicks new city and checks if it is triggered', () => {
  const selectCity = jest.fn();

  render(<Nav selectCity={selectCity} currentCity={cities[1]} />);
  const singapore = screen.getByText(/singapore/i);
  fireEvent.click(singapore);

  expect(selectCity).toHaveBeenCalledWith(cities[2]);
});
