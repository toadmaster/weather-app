import './styles.scss';

interface WeatherCellProps {
  day: number;
  title: string;
  IconComponent: ImportedSVG;
  temperature: number;
  status: string;
}

const WeatherCell = ({ day, title, IconComponent, temperature, status }: WeatherCellProps) => {
  return (
    <section className={`weather-card weather-card--day${day}`}>
      <h2>{day === 0 ? 'Today' : title}</h2>
      {day === 0 ? (
        <div className="weather-card__weather-day0">
          <div className="weather-card__icon--big">
            <IconComponent />
          </div>
          <div>
            <div className="weather-card__temperature">{temperature}&deg;</div>
            <span className="weather-card__status">{status}</span>
          </div>
        </div>
      ) : (
        <>
          <div className="weather-card__icon--small">
            <IconComponent />
          </div>
          <div className="weather-card__temperature weather-card__temperature--small">{temperature}&deg;</div>
        </>
      )}
    </section>
  );
};

export default WeatherCell;
