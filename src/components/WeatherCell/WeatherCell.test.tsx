import renderer from 'react-test-renderer';
import { render, screen } from '@testing-library/react';
import WeatherCell from '.';
import { ReactComponent as Icon01d } from '../../assets/icons/01d.svg';
import { ReactComponent as Icon01n } from '../../assets/icons/01n.svg';

it('renders a weather cell and matches snapshot', () => {
  const loader = renderer
    .create(<WeatherCell day={0} title="Wed" IconComponent={Icon01d} temperature={20} status="Cloudy" />)
    .toJSON();

  expect(loader).toMatchSnapshot();
});

test('renders the cell with all the necessary info (day 0 or today)', () => {
  render(<WeatherCell day={0} title="Wed" IconComponent={Icon01d} temperature={20} status="Cloudy" />);

  const dayName = screen.getByText(/today/i);
  const dayClass = document.querySelector('.weather-card--day0');
  const temperature = screen.getByText(/20°/i);
  const status = screen.getByText(/cloudy/i);
  const svg = document.querySelector('.weather-card__icon--big svg');

  expect(dayName).toBeInTheDocument();
  expect(dayClass).toBeInTheDocument();
  expect(temperature).toBeInTheDocument();
  expect(status).toBeInTheDocument();
  expect(svg).toBeInTheDocument();
});

test('renders the cell with all the necessary info (day 1)', () => {
  render(<WeatherCell day={1} title="Wed" IconComponent={Icon01n} temperature={21} status="Sunny" />);

  const dayName = screen.getByText(/wed/i);
  const dayClass = document.querySelector('.weather-card--day1');
  const temperature = screen.getByText(/21°/i);
  const svg = document.querySelector('.weather-card__icon--small svg');

  expect(dayName).toBeInTheDocument();
  expect(dayClass).toBeInTheDocument();
  expect(temperature).toBeInTheDocument();
  expect(svg).toBeInTheDocument();
});
