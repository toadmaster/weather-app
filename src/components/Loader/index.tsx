import { ReactComponent as SVGLoader } from '../../assets/icons/hurricane.svg';

import './styles.scss';

const Loader = () => {
  return (
    <div className="loader-container">
      <SVGLoader />
    </div>
  );
};

export default Loader;
