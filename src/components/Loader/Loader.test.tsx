import renderer from 'react-test-renderer';
import Loader from '.';

it('renders loader and matches snapshot', () => {
  const loader = renderer.create(<Loader />).toJSON();
  expect(loader).toMatchSnapshot();
});
