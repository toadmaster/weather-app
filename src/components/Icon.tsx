import { FC, SVGProps, useEffect, useRef, useState } from 'react';

interface IconProps {
  name: string;
}

export const Icon = ({ name, ...rest }: IconProps): JSX.Element | null => {
  const importedIconPath = useRef<FC<SVGProps<SVGSVGElement>> | any>();
  const [loading, setLoading] = useState(false);
  useEffect((): void => {
    setLoading(true);
    const importIcon = async (): Promise<void> => {
      try {
        // Changing this line works fine to me
        importedIconPath.current = (await import(`../assets/icons/${name}.svg`)).default;
      } finally {
        setLoading(false);
      }
    };

    importIcon();
  }, [name]);

  if (!loading && importedIconPath.current) {
    return <img src={importedIconPath.current} />;
  }

  return null;
};
