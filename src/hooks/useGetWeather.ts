import useSWR from 'swr';
import { kelvinToCelsius, unixToDate } from '../utils';

export const weatherEndpoint = (lat: number, lon: number) =>
  `https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&appid=${process.env.REACT_APP_OPENWEATHERAPI_KEY}`;

export const fetcher = (url: string) => fetch(url).then((response) => response.json());

type useGetWeatherReturn = {
  data: Forecast[] | undefined;
  error: Error | undefined;
};

export const useGetWeather = (city: City): useGetWeatherReturn => {
  const { data, error } = useSWR(weatherEndpoint(city.lat, city.lon), fetcher);

  if (data) {
    const storedDates: Record<string, boolean> = {};
    const returnData = (data?.list as Forecast[])
      .filter((forecast) => {
        const [date] = forecast.dt_txt.split(' ');

        if (!storedDates[date]) {
          storedDates[date] = true;
          return true;
        }

        return false;
      })
      .map((forecast) => ({
        ...forecast,
        dt: unixToDate(forecast.dt as unknown as number),
        main: {
          ...forecast.main,
          temp: kelvinToCelsius(forecast.main.temp),
        },
      }))
      .slice(0, 5);

    return {
      data: returnData,
      error: undefined,
    };
  }

  return {
    data,
    error,
  };
};
