import { useState } from 'react';
import cities from './data/cities.json';
import { dateToDayName, iconsMap } from './utils';
import Loader from './components/Loader';
import MainLayout from './components/MainLayout';
import WeatherCell from './components/WeatherCell';
import { useGetWeather } from './hooks/useGetWeather';

const App = () => {
  const [city, setCity] = useState<City>(cities[0]);
  const { data, error } = useGetWeather(city);

  const changeCity = (city: City) => () => {
    setCity(city);
  };

  if (error) {
    return <div>{error?.message}</div>;
  }

  return (
    <MainLayout currentCity={city} selectCity={changeCity}>
      {data ? (
        data?.map((forecast: Forecast, i) => (
          <WeatherCell
            key={forecast.dt_txt}
            day={i}
            title={dateToDayName(forecast.dt)}
            IconComponent={iconsMap[forecast.weather[0].icon]}
            temperature={forecast.main.temp}
            status={forecast.weather[0].description}
          />
        ))
      ) : (
        <Loader />
      )}
    </MainLayout>
  );
};

export default App;
