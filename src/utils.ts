import { ReactComponent as Icon01d } from './assets/icons/01d.svg';
import { ReactComponent as Icon01n } from './assets/icons/01n.svg';
import { ReactComponent as Icon02d } from './assets/icons/02d.svg';
import { ReactComponent as Icon02n } from './assets/icons/02n.svg';
import { ReactComponent as Icon03d } from './assets/icons/03d.svg';
import { ReactComponent as Icon03n } from './assets/icons/03n.svg';
import { ReactComponent as Icon04d } from './assets/icons/03d.svg';
import { ReactComponent as Icon04n } from './assets/icons/04n.svg';
import { ReactComponent as Icon09d } from './assets/icons/09d.svg';
import { ReactComponent as Icon09n } from './assets/icons/09n.svg';
import { ReactComponent as Icon10d } from './assets/icons/10d.svg';
import { ReactComponent as Icon10n } from './assets/icons/10n.svg';
import { ReactComponent as Icon11d } from './assets/icons/11d.svg';
import { ReactComponent as Icon11n } from './assets/icons/11n.svg';
import { ReactComponent as Icon13d } from './assets/icons/13d.svg';
import { ReactComponent as Icon13n } from './assets/icons/13n.svg';
import { ReactComponent as Icon50d } from './assets/icons/50d.svg';
import { ReactComponent as Icon50n } from './assets/icons/50n.svg';

export const unixToDate = (unix: number) => new Date(unix * 1000);

export const kelvinToCelsius = (kelvin: number) => Math.ceil(kelvin - 273.15);

export const dateToDayName = (date: Date) => {
  const days = ['Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat'];
  return days[date.getDay()];
};

export const iconsMap: Record<string, ImportedSVG> = {
  '01d': Icon01d,
  '01n': Icon01n,
  '02d': Icon02d,
  '02n': Icon02n,
  '03d': Icon03d,
  '03n': Icon03n,
  '04d': Icon04d,
  '04n': Icon04n,
  '09d': Icon09d,
  '09n': Icon09n,
  '10d': Icon10d,
  '10n': Icon10n,
  '11d': Icon11d,
  '11n': Icon11n,
  '13d': Icon13d,
  '13n': Icon13n,
  '50d': Icon50d,
  '50n': Icon50n,
};
